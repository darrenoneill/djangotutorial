import datetime
import logging
from django.views import generic


class BasePageView(generic.TemplateView):
    def get_context_data(self, *args, **kwargs):
        ctx = super(BasePageView, self).get_context_data(*args, **kwargs)
        ctx['menu'] = self.menu
        return ctx


class HomePageView(BasePageView):
    template_name = 'staticpages/home.html'
    menu = 'home'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['today'] = datetime.datetime.now()
        logging.error({'abc': 123, 'def': 456})
        return context


class AboutUsView(BasePageView):
    template_name = 'staticpages/about.html'
    menu = 'about'