from django.conf.urls import patterns, include, url
from staticpages import views


urlpatterns = patterns(
    '',
    url(r'^home/$', views.HomePageView.as_view()),
    url(r'^about/$', views.AboutUsView.as_view(), name='about'),
)
