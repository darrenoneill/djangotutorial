from django.conf.urls import patterns, include, url
from django.contrib import admin
from staticpages.views import HomePageView
from django.conf import settings

urlpatterns = patterns(
    '',
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^pages/', include('staticpages.urls')),
    url(r'^articles/', include('articles.urls'))
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}),
    )