from django.conf.urls import patterns, include, url
from articles import views


urlpatterns = patterns(
    '',
    url(r'^list/$', views.ArticleListView.as_view(), name='list-articles'),
    url(r'^view/(?P<slug>[\w\-]+)/', views.ArticleDetailView.as_view(),
        name='detail-article'),
    url(r'^update/(?P<pk>\d+)/', views.ArticleUpdateView.as_view(),
        name='update-article'),
    url(r'^create/$', views.ArticleCreateView.as_view(),
        name='create-article'),
)
