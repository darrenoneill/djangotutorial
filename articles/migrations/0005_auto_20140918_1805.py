# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_remove_article_thumbnail'),
    ]

    operations = [
        migrations.CreateModel(
            name='Edition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edition_number', models.IntegerField()),
                ('edition_name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='article',
            name='edition',
            field=models.ForeignKey(to='articles.Edition', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='source',
            field=models.CharField(max_length=100, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to=b'thumbs', blank=True),
            preserve_default=True,
        ),
    ]
