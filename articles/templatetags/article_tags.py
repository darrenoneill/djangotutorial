from django import template
from django.template.defaultfilters import stringfilter


register = template.Library()

@stringfilter
@register.filter
def reverse_text(value):
    return value[::-1]