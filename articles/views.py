import datetime
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import get_user_model
from articles import models
from articles import forms


class BaseArticleView(generic.View):
    def get_context_data(self, *args, **kwargs):
        ctx = super(BaseArticleView, self).get_context_data(*args, **kwargs)
        ctx['menu'] = self.menu
        return ctx


class ArticleListView(BaseArticleView, generic.ListView):
    template_name = 'articles/list_articles.html'
    model = models.Article
    paginate_by = 20
    context_object_name = 'articles'
    menu = 'articles'

    def get_queryset(self):
        #qs = super(ArticleListView, self).get_queryset()

        # Return only published articles
        #qs = qs.filter(published=True)

        # Return articles by oneilld@google.com
        #qs = qs.filter(created_by__email='oneilld@google.com')

        #qs = qs.filter(published=True).filter(
        #    created_by__email='oneilld@google.com')

        # Return articles published in the last week
        last_week = datetime.datetime.now() - datetime.timedelta(weeks=1)
        #qs = qs.filter(created__gte=last_week)
        qs = models.Article.entities.filter(created__gte=last_week)
        return qs


class ArticleDetailView(BaseArticleView, generic.DetailView):
    template_name = 'articles/detail_article.html'
    model = models.Article
    context_object_name = 'article'
    menu = 'articles'

    def get_object(self, **kwargs):
        return models.Article.entities.get(slug=self.kwargs['slug'])


class ArticleUpdateView(BaseArticleView, generic.UpdateView):
    menu = 'articles'
    model = models.Article
    template_name = 'articles/update_article.html'
    form_class = forms.UpdateArticleForm
    success_url = reverse_lazy('list-articles')


class ArticleCreateView(BaseArticleView, generic.CreateView):
    menu = 'articles'
    model = models.Article
    template_name = 'articles/create_article.html'
    form_class = forms.UpdateArticleForm
    success_url = reverse_lazy('list-articles')

    def form_valid(self, form):
        terms = form.cleaned_data['terms']
        if terms.lower() == 'i agree':
            form.instance.published = True

        form.instance.created_by = get_user_model().objects.get(
            email='oneilld@google.com')
        return super(ArticleCreateView, self).form_valid(form)
