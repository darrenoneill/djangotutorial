from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from articles import managers


class Edition(models.Model):
    edition_number = models.IntegerField()
    edition_name = models.CharField(max_length=100)

    def __unicode__(self):
        return '{0} - {1}'.format(unicode(self.edition_number),
                                  self.edition_name)


class Article(models.Model):
    title = models.CharField(max_length=100)
    short_description = models.TextField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    published = models.BooleanField(default=False)
    slug = models.SlugField(null=True, editable=False)
    edition = models.ForeignKey(Edition, null=True)
    source = models.CharField(max_length=100, null=True, blank=True)
    thumbnail = models.ImageField(upload_to='thumbs', null=True, blank=True)

    objects = models.Manager()
    entities = managers.ArticleManager()

    def __unicode__(self):
        return self.title

    def save(self, **kwargs):
        self.slug = slugify(self.title)
        return super(Article, self).save(**kwargs)


