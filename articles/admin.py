from django.contrib import admin
from articles import models


admin.autodiscover()
admin.site.register(models.Article)
admin.site.register(models.Edition)
