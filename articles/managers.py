from django.db import models


class ArticleManager(models.Manager):

    def get_queryset(self):
        qs = super(ArticleManager, self).get_queryset()
        qs = qs.filter(published=True)
        return qs