from django import forms
from articles import models


class UpdateArticleForm(forms.ModelForm):

    terms = forms.CharField(required=True)

    class Meta():
        model = models.Article
        fields = ('title', 'short_description', 'edition', 'thumbnail',
                  'terms')
